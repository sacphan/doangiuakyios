//
//  AddGuestViewController.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/19/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import UIKit
protocol AddGuestDelegate
{
    func AddGuest(_ guest :Guest)
    func EditGuest(_ guest:Guest , _ index:Int)
}
class AddGuestViewController: UIViewController {

    @IBOutlet weak var fistnameTxt: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var lastnameTxt: UITextField!
    @IBOutlet weak var GuestTxt: UITextField!
    @IBOutlet weak var tableTxt: UITextField!
    @IBOutlet weak var sectionTxt: UITextField!
    var guestCurrent:Guest?
    var indexGuestCurrent:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.textAlignment = .center
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
                  self.view.addGestureRecognizer(tapGesture)
        if (guestCurrent != nil && indexGuestCurrent != nil)
        {
            fistnameTxt.text! = guestCurrent!.FistName
            lastnameTxt.text! = guestCurrent!.LastName
            GuestTxt.text! = String(guestCurrent!.Guests)
            tableTxt.text! = guestCurrent!.Table
            sectionTxt.text! = guestCurrent!.Section
        }
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        fistnameTxt.resignFirstResponder()
        lastnameTxt.resignFirstResponder()
        GuestTxt.resignFirstResponder()
        tableTxt.resignFirstResponder()
        sectionTxt.resignFirstResponder()
         
    }
    var delegate:AddGuestDelegate?

    @IBAction func saveGuest(_ sender: Any) {
        let guest=Guest()
        guest.FistName=fistnameTxt.text!
        guest.LastName=lastnameTxt.text!
        guest.Guests=Int(GuestTxt.text!=="" ? "0" : GuestTxt.text!)!
        guest.Table=tableTxt.text!
        guest.Section=sectionTxt.text!
        if (guestCurrent == nil && indexGuestCurrent == nil)
        {
            delegate?.AddGuest(guest)
        }
        else
        {
            guest.ID = guestCurrent!.ID
            guest.IDEvent = guestCurrent!.IDEvent
            delegate?.EditGuest(guest,indexGuestCurrent!)
            
        }
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
