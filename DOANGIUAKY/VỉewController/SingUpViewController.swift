//
//  SingUpViewController.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/30/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import UIKit
import FirebaseAuth

import FirebaseFirestore
class SingUpViewController: UIViewController {

    @IBOutlet weak var firstnameTxt: UITextField!
    @IBOutlet weak var lastnameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var passwordConfirmTxt: UITextField!
    
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var errorLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()

        // Do any additional setup after loading the view.
    }
    func setupElements()
    {
        errorLbl.alpha = 0
        Utilities.styleTextField(firstnameTxt)
        Utilities.styleTextField(lastnameTxt)
        Utilities.styleTextField(emailTxt)
        Utilities.styleTextField(passwordTxt)
        Utilities.styleTextField(passwordConfirmTxt)
        Utilities.styleFilledButton(signupBtn)
    }
    func ValidationFields() -> String?
    {
        if (firstnameTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)=="" ||
            lastnameTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)=="" ||
            emailTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)=="" ||
            passwordTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)=="" ||
            passwordConfirmTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)==""
            )
        {
            return "Please fill in all fields"
        }
        let CleanedPassword = passwordTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if (Utilities.isPasswordValid(CleanedPassword)==false)
        {
            return "Please make sure you password is at least 8 character, contains special character and a number"
        }
        if (passwordConfirmTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines) != passwordConfirmTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            return "Password  confirm don't correct"
        }
        return nil
    }
    @IBAction func signupAction(_ sender: Any) {
        let error = ValidationFields()
        if (error != nil)
        {
            showError(error!)
        }
        else
        {
            let firstname = firstnameTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastname = lastnameTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
             
                let db = Firestore.firestore()
                db.collection("Users").addDocument(data: [
                    "fistname" : firstname ,
                    "lastname" : lastname
                ]) { (err) in
                    if (err != nil)
                    {
                        self.showError("Error create user data")
                    }
                    else
                    {
                        self.transitionHome()
                    }
                }
            }
        }
    }
    func transitionHome()
    {
        let Homeview = (self.storyboard?.instantiateViewController(identifier: Constants.StoryBoard.HomeViewController) as? ViewController)!
        let rootView = UINavigationController(rootViewController: Homeview)
        view.window?.rootViewController = rootView
        view.window?.makeKeyAndVisible()
    }
    func showError(_ message:String)
    {
        errorLbl.text! = message
        errorLbl.alpha = 1
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
