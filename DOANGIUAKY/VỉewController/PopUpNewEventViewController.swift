//
//  PopUpNewEventViewController.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/16/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import UIKit

class PopUpNewEventViewController: UIViewController {

    @IBOutlet weak var messagelbl: UILabel!
    @IBOutlet weak var childview: UIView!
    @IBOutlet weak var titlebtn: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor=UIColor.black.withAlphaComponent(0.8)
        messagelbl.text!="Creating new event will delete all the info from previous or current event on the app"
        messagelbl.textAlignment = .center
        messagelbl.numberOfLines = 0
        childview.layer.cornerRadius=10
        titlebtn.layer.cornerRadius=35
        titlebtn.layer.borderWidth=1
        titlebtn.layer.borderColor=UIColor.white.cgColor
        // Do any additional setup after loading the view.
    }
    
    @IBAction func OKbtn(_ sender: Any) {
        let CreateEventView=self.storyboard?.instantiateViewController(identifier: "CreateNewEvent") as! CreateNewEventViewController
        self.navigationController?.pushViewController(CreateEventView, animated: true)
        self.view.removeFromSuperview()
    }
    
    @IBAction func Canclebtn(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
