//
//  CreateNewEventViewController.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/16/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import UIKit
import IGColorPicker

class CreateNewEventViewController: UIViewController,UITextFieldDelegate,UIFontPickerViewControllerDelegate,ColorPickerViewDelegate,ColorPickerViewDelegateFlowLayout,AddGuestDelegate,UITableViewDataSource,UITableViewDelegate {
 
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listguest.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell=TBVIEW.dequeueReusableCell(withIdentifier: "CELL") as! EventTableViewCell
        if (indexPath.row==0)
        {
        
            cell.namelbl.text!="Name"
            cell.guestLbl.text!="Guest"
            cell.tableLbl.text!="Table"
            cell.sectionLbl.text!="Section"
           
            
        }
        else
        {
            cell.namelbl.text!=listguest[indexPath.row-1].FistName
            cell.guestLbl.text!=String(listguest[indexPath.row-1].Guests)
            cell.tableLbl.text!=listguest[indexPath.row-1].Table
            cell.sectionLbl.text!=listguest[indexPath.row-1].Section
        }
       
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addguestcontroller=self.storyboard?.instantiateViewController(identifier: "ADDGUEST") as! AddGuestViewController
              addguestcontroller.delegate = self
        addguestcontroller.guestCurrent=listguest[indexPath.row-1]
        addguestcontroller.indexGuestCurrent=indexPath.row
              self.navigationController?.pushViewController(addguestcontroller, animated: true)
    }
    func AddGuest(_ guest: Guest) {
        print("Addguest")
        listguest.append(guest)
        TBVIEW.reloadData()
    }
    func EditGuest(_ guest: Guest,_ index:Int) {
            print(index)
           listguest[index-1]=guest
           TBVIEW.reloadData()
       }
    
   
   
    
    var listguest:Array<Guest>=[]
    @IBOutlet weak var TBVIEW: UITableView!
    @IBOutlet weak var titilelbl: UILabel!
    @IBOutlet weak var eventnametxt: UITextField!
    @IBOutlet weak var selectfontBtn: UIButton!
    @IBOutlet weak var fontLbl: UILabel!
    @IBOutlet weak var fontsizeLbl: UILabel!
    @IBOutlet weak var fontcolorLbl: UILabel!
    @IBOutlet weak var displayrecord: UILabel!
    @IBOutlet weak var fontsizeVal: UILabel!
    var curentEvent:Event?
    var colorpickerview : ColorPickerView!
    func fontPickerViewControllerDidPickFont(_ viewController: UIFontPickerViewController) {
        
        guard let descriptor = viewController.selectedFontDescriptor else { return }
        fontLbl.font=UIFont(descriptor: descriptor, size: 17)
        fontLbl.text!=descriptor.postscriptName
        eventnametxt.font = UIFont(descriptor: descriptor, size: eventnametxt.font!.pointSize)
       
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        titilelbl.text!="CREATE NEW EVENT"
        titilelbl.textAlignment = .center
        displayrecord.textAlignment = .center
        displayrecord.text!="DISPLAY RECORDS "
        //let tapGesture=UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        //self.view.addGestureRecognizer(tapGesture)
        eventnametxt.delegate=self
        TBVIEW.dataSource=self
        TBVIEW.delegate = self
        if (curentEvent != nil)
        {
            eventnametxt.text! = curentEvent!.name
            fontLbl.text! = curentEvent!.font
            fontsizeVal.text! = String(curentEvent!.fontSize)
           
            fontcolorLbl.backgroundColor = UIColor(ciColor: CIColor(string: curentEvent!.fontColor))
             eventnametxt.textColor = fontcolorLbl.backgroundColor
            eventnametxt.font = UIFont(name: fontLbl.text!, size: CGFloat(Float(fontsizeVal.text!)!))
            listguest = DBGuest.selectByIDEvent(curentEvent!.ID)
            for item in listguest {
                print("\n" + String(item.IDEvent) + "\n")
            }
            
        }
       
        // Do any additional setup after loading the view.
    }
    
   
    @IBAction func selectfont(_ sender: Any) {
        let fontpicker=UIFontPickerViewController()
        fontpicker.delegate=self
        self.present(fontpicker, animated: true, completion:nil  )
        
    }
    @IBAction func sliderchange(_ sender: UISlider) {
        sender.value = round(sender.value)
        fontsizeVal.text! = sender.value.description
        eventnametxt.font=UIFont(name: eventnametxt!.font!.fontName, size: CGFloat(sender.value))
    }
    @IBAction func fontcolor(_ sender: Any) {
         colorpickerview=ColorPickerView(frame: CGRect(x: self.view.frame.width/4, y:self.view.frame.height/2, width: 200, height: 200))
           self.view.addSubview(colorpickerview)
        colorpickerview.delegate=self
        colorpickerview.layoutDelegate=self
        colorpickerview.style = .circle
        colorpickerview.selectionStyle = .none
        //colorpickerview.scrollToPreselectedIndex = true
        colorpickerview.isSelectedColorTappable = false
        colorpickerview.preselectedIndex = colorpickerview.colors.indices.first
        colorpickerview.backgroundColor=UIColor.white
         
    }
    
    override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
      }
    func colorPickerView(_ colorPickerView: ColorPickerView, didSelectItemAt indexPath: IndexPath) {
           fontcolorLbl.backgroundColor!=colorPickerView.colors[indexPath.item]
        eventnametxt.textColor = colorPickerView.colors[indexPath.item]
           colorpickerview.removeFromSuperview()
         
           
       }
     func colorPickerView(_ colorPickerView: ColorPickerView, didDeselectItemAt indexPath: IndexPath) {
         // A color has been deselected
       }
       func colorPickerView(_ colorPickerView: ColorPickerView, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
         // Space between cells
         return 10
       }

       func colorPickerView(_ colorPickerView: ColorPickerView, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
         // Space between rows
         return 10
       }
    @IBAction func addguest(_ sender: Any) {
        let addguestcontroller=self.storyboard?.instantiateViewController(identifier: "ADDGUEST") as! AddGuestViewController
        addguestcontroller.delegate = self
        self.navigationController?.pushViewController(addguestcontroller, animated: true)
    }
    
    
    @IBAction func saveEvent(_ sender: Any) {
        //delete Event old
        if (DBEvent.selectAllEvent().first != nil && curentEvent == nil )
        {
            let eventcurrent=DBEvent.selectAllEvent().first
            let listguestOld=DBGuest.selectByIDEvent(eventcurrent!.ID)
            for item in listguestOld {
                DBGuest.DeleteGuest(GuestDelete: item)
            }
            DBEvent.DeleteEvent(EventDelete: eventcurrent!)
           
            
        }
        //Event new
        let event=Event()
        event.name = eventnametxt.text!
        event.font = fontLbl.text!
        event.fontSize = Float(fontsizeVal.text!)!
        
        event.fontColor = fontcolorLbl.backgroundColor!.toString()
        //event save
        var IDEvent: Int = 0
        if (curentEvent != nil )
        {
            IDEvent = curentEvent!.ID
            event.ID = curentEvent!.ID
            DBEvent.UpdateEvent(EventEdit: event)
        }
        else
        {
              IDEvent = DBEvent.SaveData(event: event)
             
        }
        //list guest save
        if (listguest.count > 0)
                   {
                     for guest in listguest {
                         if (guest.IDEvent == 0)
                         {
                             guest.IDEvent = IDEvent
                             DBGuest.SaveData(guest: guest)
                         }
                         else
                         {
                             DBGuest.UpdateGuest(GuestEdit: guest)
                         }
                     }
                 }
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
  

}

extension UIColor {
    func toString() -> String {
         let colorRef = self.cgColor
         return CIColor(cgColor: colorRef).stringRepresentation
    }
}
