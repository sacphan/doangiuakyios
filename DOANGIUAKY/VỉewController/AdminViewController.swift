//
//  AdminViewController.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/16/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import UIKit
import FirebaseAuth
class AdminViewController: UIViewController {

    
    @IBOutlet weak var neweventbtn: UIButton!
    @IBOutlet weak var editeventbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        neweventbtn.titleLabel?.textAlignment = .center
        neweventbtn.layer.borderWidth = 1
        neweventbtn.layer.borderColor = UIColor.blue.cgColor
        neweventbtn.layer.cornerRadius=7
        editeventbtn.titleLabel?.textAlignment = .center
        editeventbtn.layer.borderWidth = 1
        editeventbtn.layer.borderColor = UIColor.blue.cgColor
        editeventbtn.layer.cornerRadius=7
        // Do any additional setup after loading the view.
    }
    

    @IBAction func createnewevent(_ sender: Any) {
       
        let popup=self.storyboard?.instantiateViewController(identifier: "PopUpNewEvent") as! PopUpNewEventViewController
        self.addChild(popup)
        popup.view.frame=self.view.frame
        self.view.addSubview(popup.view)
        popup.didMove(toParent: self)
        
    }
    
    @IBAction func editcurrentEvent(_ sender: Any) {
        let eventCurrent = DBEvent.selectAllEvent().first!
        let CreateEventView=self.storyboard?.instantiateViewController(identifier: "CreateNewEvent") as! CreateNewEventViewController
        CreateEventView.curentEvent = eventCurrent
               self.navigationController?.pushViewController(CreateEventView, animated: true)
    }
    
    @IBAction func ExitApp(_ sender: Any) {
         let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
            let signinview = self.storyboard?.instantiateViewController(identifier: Constants.StoryBoard.LoginViewController) as? LoginViewController
            let rootView = UINavigationController(rootViewController: signinview!)
            self.view.window?.rootViewController = rootView
            
        } catch let signOutError as NSError {
          print ("Error signing out: %@", signOutError)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
