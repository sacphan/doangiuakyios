//
//  LoginViewController.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/30/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import UIKit
import FirebaseAuth
import AVKit
class LoginViewController: UIViewController {

   var videoPlayer:AVPlayer?
      
      var videoPlayerLayer:AVPlayerLayer?
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var signinBtn: UIButton!
    @IBOutlet weak var errorLbl: UILabel!
    @IBOutlet weak var passwordTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
      setUpVideo()
        // Hide toolbar and navbar
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isToolbarHidden = true

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.isToolbarHidden = false
    }
         
    
    func setupElements()
    {
        errorLbl.alpha = 0
        Utilities.styleTextField(emailTxt)
        Utilities.styleTextField(passwordTxt)
        Utilities.styleFilledButton(signupBtn)
        Utilities.styleFilledButton(signinBtn)
    }
    @IBAction func signinAction(_ sender: Any) {
        let email = emailTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTxt.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        Auth.auth().signIn(withEmail: email, password: password) { (result, err) in
            if (err != nil)
            {
                self.errorLbl.text! = err!.localizedDescription
                self.errorLbl.alpha = 1
            }
            else
            {
                print(result as Any)
                let homeview = self.storyboard?.instantiateViewController(identifier: Constants.StoryBoard.HomeViewController) as? ViewController
                let rootview = UINavigationController(rootViewController: homeview!)
                self.view.window?.rootViewController = rootview
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
    func setUpVideo() {
           
           // Get the path to the resource in the bundle
           let bundlePath = Bundle.main.path(forResource: "loginbg", ofType: "mp4")
           
           guard bundlePath != nil else {
               return
           }
           
           // Create a URL from it
           let url = URL(fileURLWithPath: bundlePath!)
           
           // Create the video player item
           let item = AVPlayerItem(url: url)
           
           // Create the player
           videoPlayer = AVPlayer(playerItem: item)
           
           // Create the layer
           videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
           
           // Adjust the size and frame
           videoPlayerLayer?.frame = CGRect(x: -self.view.frame.size.width*1.5, y: 0, width: self.view.frame.size.width*4, height: self.view.frame.size.height)
           
           view.layer.insertSublayer(videoPlayerLayer!, at: 0)
           
           // Add it to the view and play it
           videoPlayer?.playImmediately(atRate: 0.3)
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
