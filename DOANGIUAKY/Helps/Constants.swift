//
//  Constraint.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/30/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import Foundation

struct Constants
{
    struct StoryBoard
    {
        static let HomeViewController = "HomeView"
        static let LoginViewController = "LoginView"
    }
}
