//
//  DBManager.swift
//  DOANGIUAKY
//
//  Created by sacphan on 4/19/20.
//  Copyright © 2020 sacphan. All rights reserved.
//

import Foundation
import RealmSwift

 class Event:Object
{
    @objc dynamic var ID:Int=0
    @objc dynamic var name:String=""
    @objc dynamic var font:String="Arial"
    @objc dynamic var fontSize:Float=17.0
    @objc dynamic var fontColor:String="Black"
    override class func primaryKey() -> String? {
        return "ID"
    }
    init(name:String,font:String,fontSize:Float,fontColor:String) {
        self.name=name
        self.font=font
        self.fontSize=fontSize
        self.fontColor=fontColor
    }
    
    override required init() {
       
    }
    static func incrementID()-> Int{
          let realm=try! Realm()
          return (realm.objects(Event.self).max(ofProperty: "ID") as Int? ?? 0)+1
      }
}
@objcMembers class Guest:Object
{
    dynamic var ID:Int=0
    dynamic var IDEvent:Int=0
    dynamic var FistName:String=""
    dynamic var LastName:String=""
    dynamic var Guests:Int=0
    dynamic var Table:String=""
    dynamic var Section:String=""
    init(IDEvent:Int,FistName:String,LastName:String,Guests:Int,Table:String,Section:String) {
        self.IDEvent=IDEvent
        self.FistName=FistName
        self.LastName=LastName
        self.Guests=Guests
        self.Table=Table
        self.Section=Section
    }
    
    override required init() {
       
    }
    static func incrementID()-> Int{
          let realm=try! Realm()
          return (realm.objects(Guest.self).max(ofProperty: "ID") as Int? ?? 0)+1
      }
}
class DBManagerEvent
{
    func SaveData(event :Event) -> Int
    {
        let realm = try! Realm()
        let newEvent=Event.init(name:event.name,font:event.font,fontSize:event.fontSize,fontColor:event.fontColor)
        newEvent.ID=Event.incrementID()
        do
        {
            try realm.write{
                realm.add(newEvent)
            }
            return newEvent.ID
            
        }catch{
            print(error.localizedDescription)
        }
        return 0
    }
    func UpdateEvent(EventEdit: Event) -> Bool
    {
        let realm = try! Realm()
        if let event = realm.objects(Event.self).filter("ID = \(EventEdit.ID)").first
        {
            try! realm.write
            {
                event.name=EventEdit.name
                event.font=EventEdit.font
                event.fontSize=EventEdit.fontSize
                event.fontColor=EventEdit.fontColor
            }
            return true
        }
        else
        {
            return false
        }
    }
    func selectAllEvent() -> Array<Event>
    {
        let realm = try! Realm()
        let event=realm.objects(Event.self)
        return Array(event)
    }
    func DeleteEvent(EventDelete: Event) -> Bool
    {
        let realm=try! Realm()
        if let event=realm.objects(Event.self).filter("ID=\(EventDelete.ID)").first
        {
            try! realm.write
            {
                realm.delete(event)
               
            }
             return true
        }
        return false
    }
}

//Guest
class DBManagerGuest
{
    func SaveData(guest :Guest) -> Int
    {
        let realm = try! Realm()
        let newGuest=Guest.init(IDEvent: guest.IDEvent, FistName: guest.FistName, LastName: guest.LastName, Guests: guest.Guests, Table: guest.Table, Section: guest.Section)
        newGuest.ID=Guest.incrementID()
        do
        {
            try realm.write{
                realm.add(newGuest)
                
            }
          
            return newGuest.ID
        }catch{
            print(error.localizedDescription)
        }
        return 0
    }
    func UpdateGuest(GuestEdit: Guest) -> Bool
    {
        let realm = try! Realm()
        if let guest = realm.objects(Guest.self).filter("ID = \(GuestEdit.ID)").first
        {
            try! realm.write
            {
                guest.FistName=GuestEdit.FistName
                guest.LastName=GuestEdit.LastName
                guest.Guests=GuestEdit.Guests
                guest.Table=GuestEdit.Table
                guest.Section=GuestEdit.Section
            }
            return true
        }
        else
        {
            return false
        }
    }
    func selectAllGuest() -> Array<Guest>
    {
        let realm = try! Realm()
        let guest=realm.objects(Guest.self)
        return Array(guest)
    }
    func selectByIDEvent(_ IDEvent:Int) ->Array<Guest>
    {
        let realm = try! Realm()
        let guest=realm.objects(Guest.self).filter("IDEvent = \(IDEvent)")
        return Array(guest)
    }
    func DeleteGuest(GuestDelete: Guest) -> Bool
    {
        let realm=try! Realm()
        if let guest=realm.objects(Guest.self).filter("ID=\(GuestDelete.ID)").first
        {
            try! realm.write
            {
                realm.delete(guest)
              
            }
              return true
        }
        return false
    }
}

let DBEvent = DBManagerEvent()
let DBGuest = DBManagerGuest()


